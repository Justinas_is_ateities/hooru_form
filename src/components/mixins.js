export const valid = {
	methods: {
		valid(value, type) {
			if (type == 'text') {
				if (String(value).length != 0) {
					return true
				} else {
					this.formValid = false;
					return false
				}
			} else if (type == 'email') {
				const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

				if (String(value).length != 0 && re.test(String(value).toLowerCase())) {
					return true
				} else {
					this.formValid = false;
					return false
				}
			} else if (type == 'list') {
				if (String(value).length != 0) {
					return true
				} else {
					this.formValid = false;
					return false
				}
			}
		}
	}
}