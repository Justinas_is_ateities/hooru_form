import Api from '../scripts/api'

export default {
  install: async Vue => {
    let textData = {}
    
    Vue.prototype.getField = f => {
      if (typeof textData[f] !== 'undefined') return textData[f]
    }
    
    try {
      const response = await Api.call('app-translations', {language: 'en'})
      if (response.data) {
        textData = response.data
      }
    } catch(err) {

    }
  }
}
