import TermsAndConditions from './TermsAndConditions';
import Form from './Form';
import TermsAndForm from './TermsAndForm';
import UploadPhoto from './UploadPhoto';
import DocumentValidation from './DocumentValidation';
import UploadPassport from './UploadPassport';
import UploadCard from './UploadCard';
import UploadOther from './UploadOther';
import Success from './Success';
import Home from './Home';
import Login from './Login';

export const routes = [
	{ path: '/', component: Home },
	{ path: '/terms-and-form', component: TermsAndForm },
	{ path: '/terms-and-conditions', component: TermsAndConditions },
	{ path: '/form', component: Form },
	{ path: '/upload-photo', component: UploadPhoto },
	{ path: '/document-validation', component: DocumentValidation },
	{ path: '/upload-passport', component: UploadPassport },
	{ path: '/upload-card', component: UploadCard },
	{ path: '/upload-other', component: UploadOther },
	{ path: '/success', component: Success },
	{ path: '/login', name: 'Login', component: Login },
	{ path: '**', redirect: '/' }
];
