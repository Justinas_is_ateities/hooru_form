const SETTINGS = '_settings'

function storageSettings(settings) {
  if (arguments.length > 0) {
    if (settings) {
      sessionStorage.setItem(SETTINGS, JSON.stringify(settings))
    } else {
      sessionStorage.removeItem(SETTINGS)
    }
  } else {
    let value = sessionStorage.getItem(SETTINGS)
    if (value) {
      value = JSON.parse(value)
    }
    return value
  }
}

export default {
  storageSettings
}