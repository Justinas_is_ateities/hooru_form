const BASE_URL = `${window.location.origin}/api`
//const BASE_URL = 'https://testapi.ondato.com/kyc'
//const BASE_URL = 'https://localhost:44300'

function call(method, body, params) {
  return fetch(fullUrl(method), {
    method: 'POST',
    cache: 'no-cache',
    mode: 'cors',
    credentials: 'include',
    headers: { // TODO: Other parameters can be added as headers.
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  }).then(convertResponse)
  .catch(errorHandler)
}

function sendFile(method, body) {
  return fetch(fullUrl(method), {
    method: 'POST',
    cache: 'no-cache',
    mode: 'cors',
    credentials: 'same-origin',
    body
  }).then(convertResponse)
  .catch(errorHandler)
}

async function convertResponse(response) {
  if (response.redirected) {
    return
  } else if (response.status === 200) {
    return response.json()
  } else if (response.status === 400) {
    const error = {
      status: response.status
    }
    try {
      error.body = await response.json()
    } catch(err) { }
    throw error
  } else {
    throw { status: response.status }
  }
}

function errorHandler(response) {
  console.error('Request Error Handler', response);

  // TODO: Handle error body.
  throw response
}

function fullUrl(path) {
  return `${BASE_URL}/${path}`
}

export default {
  call,
  sendFile
}
